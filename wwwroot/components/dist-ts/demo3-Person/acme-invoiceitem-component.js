System.register(["/node_modules/vue/dist/vue.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, component;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            }
        ],
        execute: function () {
            component = {
                props: ['invoiceitem'],
                render: function anonymous() {
                    with (this) {
                        return _c('div', {
                            attrs: {
                                "acme-invoiceitem-component": ""
                            }
                        }, [_v("\n    " + _s(invoiceitem) + "\n")]);
                    }
                },
                staticRenderFns: []
            };
            vue_js_1.default.component("acme-invoiceitem", component);
        }
    };
});
//# sourceMappingURL=acme-invoiceitem-component.js.map