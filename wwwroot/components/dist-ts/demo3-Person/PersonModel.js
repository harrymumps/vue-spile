System.register([], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var PersonModel, InvoiceItem, Invoice, Person;
    return {
        setters: [],
        execute: function () {
            PersonModel = (function () {
                function PersonModel() {
                }
                PersonModel.newTimeId = function () {
                    return new Date().toISOString();
                };
                PersonModel.newGuidId = function () {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                    }
                    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
                };
                PersonModel.newMiniGuidId = function () {
                    function s4() {
                        return Math.floor((1 + Math.random()) * 0x10000)
                            .toString(16)
                            .substring(1);
                    }
                    return s4() + '-' + s4();
                };
                PersonModel.newId = function () {
                    return PersonModel.newMiniGuidId();
                };
                PersonModel.createInvoiceItem = function (x) {
                    return InvoiceItem.create(PersonModel.newId());
                };
                PersonModel.createInvoice = function () {
                    return Invoice.create({ id: PersonModel.newId() });
                };
                PersonModel.getPersonData = function () {
                    return Person.create();
                };
                return PersonModel;
            }());
            exports_1("PersonModel", PersonModel);
            InvoiceItem = (function () {
                function InvoiceItem() {
                }
                InvoiceItem.create = function (x) {
                    return {
                        id: x.id,
                        ref: PersonModel.newId(),
                        amount: 100,
                        taxRate: 0.2,
                        n: 10,
                    };
                };
                return InvoiceItem;
            }());
            exports_1("InvoiceItem", InvoiceItem);
            Invoice = (function () {
                function Invoice() {
                }
                Invoice.create = function (x) {
                    return {
                        id: PersonModel.newId(),
                        ref: PersonModel.newId(),
                        invoiceItems: [
                            PersonModel.createInvoiceItem({ id: PersonModel.newId() }),
                            PersonModel.createInvoiceItem({ id: PersonModel.newId() }),
                            PersonModel.createInvoiceItem({ id: PersonModel.newId() })
                        ],
                    };
                };
                return Invoice;
            }());
            exports_1("Invoice", Invoice);
            Person = (function () {
                function Person() {
                }
                Person.create = function () {
                    return {
                        id: PersonModel.newId(),
                        surname: 'Smith',
                        forename: 'John',
                        invoices: [
                            PersonModel.createInvoice(),
                            PersonModel.createInvoice(),
                        ]
                    };
                };
                return Person;
            }());
            exports_1("Person", Person);
        }
    };
});
//# sourceMappingURL=PersonModel.js.map