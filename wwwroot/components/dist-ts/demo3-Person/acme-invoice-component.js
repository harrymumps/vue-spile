System.register(["/node_modules/vue/dist/vue.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, component;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            }
        ],
        execute: function () {
            component = {
                props: ["invoice"],
                render: function anonymous() {
                    with (this) {
                        return _c('div', {
                            attrs: {
                                "acme-invoice-component": ""
                            }
                        }, [_c('br'), _v("\n    -- invoice-id : " + _s(invoice.id) + " : invoice-ref : " + _s(invoice.ref) + " : \n    "), _c('br'), _v("\n    --- # invoice-items : " + _s(invoice.invoiceItems.length) + " :\n    "), _l((invoice.invoiceItems), function (x) {
                                return _c('acme-invoiceitem', {
                                    key: x.id,
                                    attrs: {
                                        "invoiceitem": x
                                    }
                                });
                            })], 2);
                    }
                },
                staticRenderFns: []
            };
            vue_js_1.default.component("acme-invoice", component);
        }
    };
});
//# sourceMappingURL=acme-invoice-component.js.map