System.register(["/node_modules/vue/dist/vue.js", "./acme-person-component.js", "./acme-invoice-component.js", "./acme-invoiceitem-component.js", "./PersonModel.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, PersonModel_js_1, component;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            },
            function (_1) {
            },
            function (_2) {
            },
            function (_3) {
            },
            function (PersonModel_js_1_1) {
                PersonModel_js_1 = PersonModel_js_1_1;
            }
        ],
        execute: function () {
            component = {
                el: '#person',
                data: function () {
                    return PersonModel_js_1.PersonModel.getPersonData();
                }
            };
            window.quiz = new vue_js_1.default(component);
        }
    };
});
//# sourceMappingURL=person.js.map