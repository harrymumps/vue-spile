System.register(["/node_modules/vue/dist/vue.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, component;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            }
        ],
        execute: function () {
            component = {
                props: ["person"],
                render: function anonymous() {
                    with (this) {
                        return _c('div', {
                            attrs: {
                                "acme-invoiceitem-component": ""
                            }
                        }, [_c('h3', [_v(" " + _s(person.surname) + " " + _s(person.forename) + " \n    ")]), _v(" "), _c('b', [_v("Invoices :")]), _v(" "), _c('br'), _v(" "), _l((person.invoices), function (x) {
                                return _c('acme-invoice', {
                                    key: x.id,
                                    attrs: {
                                        "invoice": x
                                    }
                                });
                            })], 2);
                    }
                },
                staticRenderFns: []
            };
            vue_js_1.default.component("acme-person", component);
        }
    };
});
//# sourceMappingURL=acme-person-component.js.map