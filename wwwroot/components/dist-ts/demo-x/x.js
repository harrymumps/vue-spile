System.register(["/node_modules/vue/dist/vue.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, component;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            }
        ],
        execute: function () {
            component = {
                data: function () {
                    return {
                        title: '',
                        render: function anonymous() {
                            with (this) {
                                return _c('div', [_c('h2', [_v("\n        " + _s(title) + "\n    ")]), _v(" "), _m(0)]);
                            }
                        },
                        staticRenderFns: [function anonymous() {
                                with (this) {
                                    return _c('div', [_c('p', [_v("123")])]);
                                }
                            },]
                    };
                },
                created: function () { }
            };
            vue_js_1.default.component("acme-x", component);
        }
    };
});
//# sourceMappingURL=x.js.map