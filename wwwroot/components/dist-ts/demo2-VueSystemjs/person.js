System.register(["/node_modules/vue/dist/vue.js", "./acme-person.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1, component, vueComponentInstance;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            },
            function (_1) {
            }
        ],
        execute: function () {
            component = {
                el: '#person',
                data: function () {
                    return {
                        forename: 'JohnA',
                        surname: 'SmithA'
                    };
                }
            };
            vueComponentInstance = new vue_js_1.default(component);
        }
    };
});
//# sourceMappingURL=person.js.map