System.register(["/node_modules/vue/dist/vue.js"], function (exports_1, context_1) {
    var __moduleName = context_1 && context_1.id;
    var vue_js_1;
    return {
        setters: [
            function (vue_js_1_1) {
                vue_js_1 = vue_js_1_1;
            }
        ],
        execute: function () {
            new vue_js_1.default({
                el: "#person",
                emplate: null,
                data: function () {
                    return {
                        forename: "John",
                        surname: "Smith",
                        render: function anonymous() {
                            with (this) {
                                return _c('div', [_c('input', {
                                        directives: [{
                                                name: "model",
                                                rawName: "v-model:value",
                                                value: (forename),
                                                expression: "forename",
                                                arg: "value"
                                            }],
                                        domProps: {
                                            "value": (forename)
                                        },
                                        on: {
                                            "input": function ($event) {
                                                if ($event.target.composing)
                                                    return;
                                                forename = $event.target.value;
                                            }
                                        }
                                    }), _v(" "), _c('br'), _v(" "), _c('input', {
                                        directives: [{
                                                name: "model",
                                                rawName: "v-model:value",
                                                value: (surname),
                                                expression: "surname",
                                                arg: "value"
                                            }],
                                        domProps: {
                                            "value": (surname)
                                        },
                                        on: {
                                            "input": function ($event) {
                                                if ($event.target.composing)
                                                    return;
                                                surname = $event.target.value;
                                            }
                                        }
                                    }), _v(" "), _c('br'), _v("\n    " + _s(forename) + " " + _s(surname) + "\n    "), _c('hr'), _v(" "), _m(0)]);
                            }
                        },
                        staticRenderFns: [function anonymous() {
                                with (this) {
                                    return _c('i', [_v("\n        This markup is coming from the .vue file.\n        "), _c('br'), _v("\n        This .vue file markup can be stamped on any\n         html page that has a dom element with id=#person\n        "), _c('br'), _v(" "), _c('br'), _v("\n        If using the id, then the markup can only be stamped on the page once!\n        "), _c('br'), _v("\n        Alternatively, \n        use vue components to stamp a .vue component \n        more than once and use new dom element \n        e.g. <acme-person> \n    ")]);
                                }
                            },]
                    };
                }
            });
        }
    };
});
//# sourceMappingURL=hello-world.js.map