import Vue from "/node_modules/vue/dist/vue.js";

import "./acme-person.js"

let component = {
    el: '#person',
    data: function () {
        return {
            forename:'JohnA',
            surname:'SmithA'
        }
    }
};

// Ask vue to bind a vue config to the DOM element with id of "person"
// the DOM element will be a sent a new vue model {forename:"John" person:"Smith"}
// The markup for HTML, CSS, and JavaScript is all coming from the .vue component
// Different instances of the .vue component can be sent different person data.
let vueComponentInstance = new Vue(component);

// window.personComponent = vueComponentInstance;


