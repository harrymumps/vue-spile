import Vue from "/node_modules/vue/dist/vue.js";

// this component takes a property called person
let component = {
    props: ["person"],
    emplate: null,
    render: function anonymous() {
        with(this) {
            return _c('div', [_m(0), _v(" "), _c('input', {
                directives: [{
                    name: "model",
                    rawName: "v-model:value",
                    value: (person.forename),
                    expression: "person.forename",
                    arg: "value"
                }],
                domProps: {
                    "value": (person.forename)
                },
                on: {
                    "input": function($event) {
                        if ($event.target.composing) return;
                        $set(person, "forename", $event.target.value)
                    }
                }
            }), _v(" "), _c('br'), _v(" "), _c('input', {
                directives: [{
                    name: "model",
                    rawName: "v-model:value",
                    value: (person.surname),
                    expression: "person.surname",
                    arg: "value"
                }],
                domProps: {
                    "value": (person.surname)
                },
                on: {
                    "input": function($event) {
                        if ($event.target.composing) return;
                        $set(person, "surname", $event.target.value)
                    }
                }
            }), _v(" "), _c('br'), _v(" "), _m(1), _v("\n    " + _s(person.surname) + "\n    "), _c('br'), _v("\n    " + _s(person.forename) + "\n    "), _c('br'), _v("\n    " + _s(person) + "\n    "), _m(2)])
        }
    },
    staticRenderFns: [function anonymous() {
        with(this) {
            return _c('div', [_c('p', [_v("Person Form")])])
        }
    }, function anonymous() {
        with(this) {
            return _c('div', [_c('p', [_v("Person Moustache Data")])])
        }
    }, function anonymous() {
        with(this) {
            return _c('div', [_c('p', [_v("Person Footer")])])
        }
    }, ]
};

// After this next code line, 
// we can use new dom element 
// called <acme-person></acme-person>
// And we can throw it some data, 
// on the attribute caller person
// like this <acme-person v-bind:person="$data"></acme-person>
Vue.component("acme-person", component);