import Vue from "/node_modules/vue/dist/vue.js";

// bind up a view model to the element with id #person
// Note : prefix emplate with x or t, to see the markup coming from the html page or the .vue file
new Vue({
    el: "#person",
    emplate: null,
    data: function() {
        return {
            forename: "John",
            surname: "Smith",
            render: function anonymous() {
                with(this) {
                    return _c('div', [_c('input', {
                        directives: [{
                            name: "model",
                            rawName: "v-model:value",
                            value: (forename),
                            expression: "forename",
                            arg: "value"
                        }],
                        domProps: {
                            "value": (forename)
                        },
                        on: {
                            "input": function($event) {
                                if ($event.target.composing) return;
                                forename = $event.target.value
                            }
                        }
                    }), _v(" "), _c('br'), _v(" "), _c('input', {
                        directives: [{
                            name: "model",
                            rawName: "v-model:value",
                            value: (surname),
                            expression: "surname",
                            arg: "value"
                        }],
                        domProps: {
                            "value": (surname)
                        },
                        on: {
                            "input": function($event) {
                                if ($event.target.composing) return;
                                surname = $event.target.value
                            }
                        }
                    }), _v(" "), _c('br'), _v("\n    " + _s(forename) + " " + _s(surname) + "\n    "), _c('hr'), _v(" "), _m(0)])
                }
            },
            staticRenderFns: [function anonymous() {
                with(this) {
                    return _c('i', [_v("\n        This markup is coming from the .vue file.\n        "), _c('br'), _v("\n        This .vue file markup can be stamped on any\n         html page that has a dom element with id=#person\n        "), _c('br'), _v(" "), _c('br'), _v("\n        If using the id, then the markup can only be stamped on the page once!\n        "), _c('br'), _v("\n        Alternatively, \n        use vue components to stamp a .vue component \n        more than once and use new dom element \n        e.g. <acme-person> \n    ")])
                }
            }, ]
        };
    }
});