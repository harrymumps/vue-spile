import Vue from "/node_modules/vue/dist/vue.js";
let component = {
    props: ["invoice"],
    render: function anonymous() {
        with(this) {
            return _c('div', {
                attrs: {
                    "acme-invoice-component": ""
                }
            }, [_c('br'), _v("\n    -- invoice-id : " + _s(invoice.id) + " : invoice-ref : " + _s(invoice.ref) + " : \n    "), _c('br'), _v("\n    --- # invoice-items : " + _s(invoice.invoiceItems.length) + " :\n    "), _l((invoice.invoiceItems), function(x) {
                return _c('acme-invoiceitem', {
                    key: x.id,
                    attrs: {
                        "invoiceitem": x
                    }
                })
            })], 2)
        }
    },
    staticRenderFns: []
};
Vue.component("acme-invoice", component);

//<style></style>