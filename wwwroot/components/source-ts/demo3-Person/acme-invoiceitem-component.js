import Vue from "/node_modules/vue/dist/vue.js";
let component = {
    props: ['invoiceitem'],
    render: function anonymous() {
        with(this) {
            return _c('div', {
                attrs: {
                    "acme-invoiceitem-component": ""
                }
            }, [_v("\n    " + _s(invoiceitem) + "\n")])
        }
    },
    staticRenderFns: []
};
Vue.component("acme-invoiceitem", component);