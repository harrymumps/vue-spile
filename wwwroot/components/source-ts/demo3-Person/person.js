import Vue from "/node_modules/vue/dist/vue.js";

import "./acme-person-component.js"
import "./acme-invoice-component.js"
import "./acme-invoiceitem-component.js"

import { PersonModel } from "./PersonModel.js"

let component = {
    el: '#person',
    data: function () {
        return PersonModel.getPersonData();
    }
};

window.quiz = new Vue(component);
