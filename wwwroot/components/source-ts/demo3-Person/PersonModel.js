﻿class PersonModel {
    static newTimeId() {
        return new Date().toISOString();
    }

    static newGuidId() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    static newMiniGuidId() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + '-' + s4();
    }

    static newId() {
        return PersonModel.newMiniGuidId();
    }

    static createInvoiceItem(x) {
        return InvoiceItem.create(PersonModel.newId());
    }

    static createInvoice() {
        return Invoice.create({ id: PersonModel.newId()});
    }

    static getPersonData() {
        return Person.create();
    }
}

class InvoiceItem {
    static create(x) {
        return {
            id: x.id,
            ref: PersonModel.newId(),
            amount: 100,
            taxRate: 0.2,
            n: 10,
        };
    }
}

class Invoice {

    static create(x) {

        return {
            id: PersonModel.newId(),
            ref: PersonModel.newId(),
            invoiceItems: [
                PersonModel.createInvoiceItem({ id: PersonModel.newId() }),
                PersonModel.createInvoiceItem({ id: PersonModel.newId() }),
                PersonModel.createInvoiceItem({ id: PersonModel.newId() })
            ],
        };
    }
}

class Person {
    static create() {
        return {
            id: PersonModel.newId(),
            surname: 'Smith',
            forename: 'John',
            invoices: [
                PersonModel.createInvoice(),
                PersonModel.createInvoice(),
            ]
        };
    }
}

export { PersonModel, Person, Invoice, InvoiceItem }

