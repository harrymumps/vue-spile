import Vue from "/node_modules/vue/dist/vue.js";

let component = {
    props: ["person"],
    render: function anonymous() {
        with(this) {
            return _c('div', {
                attrs: {
                    "acme-invoiceitem-component": ""
                }
            }, [_c('h3', [_v(" " + _s(person.surname) + " " + _s(person.forename) + " \n    ")]), _v(" "), _c('b', [_v("Invoices :")]), _v(" "), _c('br'), _v(" "), _l((person.invoices), function(x) {
                return _c('acme-invoice', {
                    key: x.id,
                    attrs: {
                        "invoice": x
                    }
                })
            })], 2)
        }
    },
    staticRenderFns: []
};

Vue.component("acme-person", component);

//<style lang="scss" scoped ></style>
//<style lang="css" ></style>