/*
With Node 9.5.0 and Chrome 64.0.3282.186
To debug the workings of vuemux with Chrome dev tools :-
at the command line enter
  >node --inspect demo-node-debugging.js
  >node --inspect inspect-node-debugging.js
  >node --inspect client-example.js
Open Chrome
Goto the URL "about:inspect"
Click [Open Dedicate devtools]

See https://medium.com/the-node-js-collection/debugging-node-js-with-google-chrome-4965b5f910f4

*/
//debugger;

let a = 1
console.log(`a:${a}`)

let b = 2
console.log(`b:${b}`)

let add = (a, b) => a + b

let partial = (fn, ...preAppliedArgs) => {
    return (...otherArgs) => fn(...preAppliedArgs, ...otherArgs)
}

let c = add(1, 2)
console.log(`c:${c}`)

let add3 = partial(add, 3)

let ate = add3(5)
console.log(ate)