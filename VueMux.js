const fs = require('fs')
const path = require('path')

const compiler = require('vue-template-compiler')
const EventEmitter = require('events')

const VueSpilerCore = require('./VueSpileCore').core
const VueSpiler = require('./VueSpile').VueSpiler

class VueMux extends EventEmitter {

    constructor() {

        super();

        // bind emit event of this class  to a subscriber fun
        let VuefileChangeObserver = VueSpiler
        this.on(VueSpilerCore.K_VUEMUX_FILE_CHANGE_EVENT, VuefileChangeObserver.onFileChange)
    }

    start() {

        let vueSpileConfig = VueSpilerCore.getVueSpileConfig()

        // when a file changes, inform the vue munger ; vueMux = this 
        var vueFileChangeSubscriber = (eventType, fileName) => {

            let message = VueSpilerCore.createMessage()

            let meta = VueSpilerCore.objectAssign(message, {
                fileName
            }, {
                eventType
            })

            this.emit(VueSpilerCore.K_VUEMUX_FILE_CHANGE_EVENT, meta);
        }

        let startVueSpileWatcher = (vueSpileConfig) => {

            let fsWatcher = fs.watch(vueSpileConfig.rootDir, {
                recursive: true
            }, vueFileChangeSubscriber)
        }

        // start the vue file watcher
        console.log(`Starting vue-spile ...`)
        console.log(vueSpileConfig)

        // start watching for changes in vue file, and transpile them tp .js files when they do change
        startVueSpileWatcher(vueSpileConfig)
    }
}

exports.VueMux = VueMux