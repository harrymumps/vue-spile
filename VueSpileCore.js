const fs = require('fs')
const path = require('path')

core = {}

core.K_OK = true
core.K_FAIL = false
core.K_UNKNOWN_ID = `_`
core.K_VUEMUX_FILE_CHANGE_EVENT = `VueMux::fileChange`

core.K_CL_NONE = 0
core.K_CL_PROD = 1
core.K_LEVEL_2 = 2
core.K_CL_DEV = 9

core.partial = (fn, ...preAppliedArgs) => {
    // this fun takes a an arg which is a function and any amout of other arguments, called them ...args
    // a function is returned from this function , 
    //  it takes another set of arguments, call them otherArgs
    //  the function returned get called called with ...otherArfs, BUT it sends in the first list of arguments ...args 
    //  and then sends in the 2nd list of arguments at the time of the call of this returned function
    return (...otherArgs) => fn(...preAppliedArgs, ...otherArgs)
}

core.createMessage = () => {
    return {}
}

core.objectAssign = (o, ...props) => {
    o = (o == null) ? {} : o
    o = (typeof o != 'object') ? {} : o
    return Object.assign(o, ...props)
}

core.consoleClear = () => {
    process.stdout.write('\x1B[2J\x1B[0f');
}

core.consoleClearPrint = (m) => {
    consoleClear()
    console.log(m);
}

core.createResult = (success = null, fail = null, status = K_OK, id = K_UNKNOWN_ID) => {
    // must pass a success packet to  to be happy-pat : if success is falsy, then its a fail
    status = success ? status : core.K_FAIL
    if (success) {
        return {
            success,
            status,
            id
        }
    } else {
        // sad path, default the fail packet to something
        fail = fail ? fail : {}
        return {
            fail,
            status,
            id
        }
    }
}

core.success = (success = {}, id = core.K_UNKNOWN_ID) => {
    return core.createResult(success, null, core.K_OK, id)
}

core.fail = (fail = {}, id = core.K_UNKNOWN_ID) => {
    return core.createResult(null, fail, core.K_FAIL, id)
}

core.getJsonFile = filePath => {
    try {
        let fileContent = fs.readFileSync(filePath, `utf8`)
        return core.success(fileContent)
    } catch (e) {
        return core.fail()
    }
}

core.getVueSpileConfigOverrideJson = (filePath = `vue-spile.json`) => {
    let jsonFileRet = core.getJsonFile(filePath)
    if (jsonFileRet.status === core.K_OK) {
        return jsonFileRet.success
    } else {
        return `{}`
    }
}

core.getVueSpileConfigOverride = () => {
    let jsonString = core.getVueSpileConfigOverrideJson()
    try {
        return JSON.parse(jsonString)
    } catch (e) {
        return {}
    }
}

core.vueSpileDefaultConfig = () => {
    //'x-consoleLevel': core.K_CL_DEV,
    //'consoleLevel': core.K_CL_PROD,
    return {
        'rootDir': `.`,
        'styleFileOutput': `.`,
        'consoleLevel': core.K_CL_DEV,
}
}

core.getVueSpileConfig = () => {
    const vueSpileConfigOverride = core.getVueSpileConfigOverride()
    return Object.assign(core.vueSpileDefaultConfig(), vueSpileConfigOverride)
}

core.cl = (msg) => {
    const vueSpileConfig = core.getVueSpileConfig()
    const configuredLevel = vueSpileConfig.consoleLevel;
    if (msg.level <= configuredLevel) {
        console.log(msg.msg)
    }
}

// exports
exports.core = core