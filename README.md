## Vue-Spile : Transpile .vue files to .js files

Vue-spile intention is to provide an alternative to webpack for using .vue files and component development. SystemJS will be used for loading javascript browser resources.
 
 + vue-spile transpiles .vue files to .js files. 

 + vue-spile assumes you have more than 1 .vue file, so by default it watches for changes to .vue files and transpiles to .js/.scss files as you develop.  
 
 + If necessary, it will also create a .scss style file from the style portion of a .vue file.

 + It can also copy the .scss style files to a separate position, so they can be included in another 'main.css' web site file.




### Pre-requisites : 

Both Node and npm are required to use vue-spile. 

Node JS will include npm when it is installed. 

Vue-spile was developed with Node (v9.5.0) and NPM(5.5.1) .

The development currently has developed with Node (v9.8.0) and NPM(5.6.0).


### npm init

You will have initialised a folder with a project.json. The best way of doing this is by the npm init command.

```
$ npm init
```


### Installation of vue-spile

Using node tooling, install vue-spile into your project :-


```
$ npm install vue-spile --save-dev
```

or you can use yarn :-

```
$ yarn add vue-spile 
```

Your package.json will also be modified with a reference to vue-spile.
Vue-spile will be added to your ./node_modules directory.
When you subsequently use the npm install command, npm can ensure the /node_modules contains the vue-spile tooling by using the reference in the package.json. 


### Usage

The following outlines several demos for using vue-spile.

> Demo #1 : start vue-spile watching for .vue file changes in the current directory and in sub-directories. When a .vue file is changed, a transpiled .js will be created or updated.

> Demo #2 : make use of a transpiled .vue file on a web page. System.js will be used not webpack.

> Demo #3 : make use of a transpiled several .vue files, which are vue components, on a web page using System.js.

The code for these demos are included in th /node_modules/vue-spile/wwwroot/components/source-ts directory.

### Demo #1 : vue-spile hello-world

##### Summary : use vscode for an editor ; npm init ; npm install vue-spile --save-dev ; node run vue-spile-client.js ; touch hello-world.vue.

 0. Create a new folder, call it vsdemo
 0. open vscode (Visual Studio Code) or another code editor. 
 0. make  vsdemo the current directory 
      ``` 
      $ cd vsdemo
      ```
 0. create a project.json file, by using the npm init command, accept all the defaults, including yes for the last question.

    ``` 
    $ npm init
    ```

  0. install vue-spile into node-modules and record the dependency in package.json
      ```
      $ npm install vue-spile --save-dev    
      ```
      alternatively, use yarn ...

      ```
      $ yarn add vue-spile     
      ```

 0. Create a node JavaScript file called vue-spile-client.js. and add this JavaScript code
 
    ```
    const VueSpile = require("vue-spile").VueMux 

    // watch for .vue file changes, transpile to .js files
    let vueSpile = new VueSpile()
    vueSpile.start()

    ```

  0. Run this file with node, by typing the following at the prompt.  
      ```
      $ node vue-spile-client.js
      ```
   
  0. Add a file called hello-world.vue, and this add this text to the file ...  
      ```

<template>
     <div>    
        <input v-model:value="forename" >
        <br>
        <input v-model:value="surname" >
        <br>
        {{forename}} {{surname}}
    </div>
</template>

<script>
  import Vue from "/node_modules/vue/dist/vue.js";

    // bind up a view model to the element with id #person
    // Note : prefix emplate with x or t, to see the markup coming from the html page or the .vue file
    new Vue({
      el: "#person",
      emplate:null,
      data: function() {
        return {
          forename: "John",
          surname: "Smith"
        };
      }
    });

</script>


      ```

if you are using VSCode, then VS code may ask you to install the Vetur plugin.  This will plugin light-up  .vue files when editing in VSCode.

  Vue-spile is watching for changes to any .vue file. When a .vue file is changed the .vue file will be transpiled to a .js file.

  Next,  How do we get those transpiled .js files onto a HTML page in a web browser?
  See demo #2.

  

### Demo #2 : Vue, SystemJs, TypeScript transpilation.

##### Summary : we will use lite-server for a simple web server ; vue-spile will transpile .vue files to .js files ; Typescript will transpile those .js files to ES5 JavaScript code with system module format ; A html page will use SystemJs to load up vue components on a web page.  




0. npm install babel-polyfill --save
0. npm install systemjs --save
0. npm install typescript --save-dev
0. $ md wwwroot ; cd wwwroot
0. $ md components ; md dist-ts ; md source-ts
0. $ cd source-ts ; md demo1-spile ; md demo2-VueSystemjs ; md demo3-Person ; 
0. move vue-spile-client.js to wwwroot
0. move hello-world.vue to wwwroot/components/demo1-spile
0. cd to ./vsdemo, the root of this demo project 
0. $ npm install lite-server
0. $ lite-server ; 
0. start a web server,  point a a browser at  http://localhost:3000/
0. create a web page ./vsdemo/index.html and paste the following code ...

      ```
        <a href="wwwroot/components/source-ts/demo1-spile/hello-vue.html">
          wwwroot/components/source-ts/demo1-spile/hello-vue.html
        </a>
        <br>
        <a href="wwwroot/components/source-ts/demo2-VueSystemjs/person.html">
            wwwroot/components/source-ts/demo2-VueSystemjs/person.html
        </a>

      ```
0. lite-server should pick the changed file and present you with the links.
0. create /wwwroot/components/source-ts/demo1-spile/hello-vue.html, 

        ```
          <!DOCTYPE html>
          <html lang="en">

          <head>
              <title></title>
              <meta charset="utf-8">
              <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
              <script src="/node_modules/babel-polyfill/dist/polyfill.min.js"></script>
              <script src="/node_modules/systemjs/dist/system.js"></script>
          </head>

          <body>

              <div id="person" v-cloak>    
                  <input v-model:value="forename" >
                  <br>
                  <input v-model:value="surname" >
                  <br>
                  {{forename}} {{surname}}
              </div>

              <script>
                  System.import("/wwwroot/components/dist-ts/demo1-spile/hello-world.js");
              </script>

          </body>
          </html>

        ```

0. The link http://localhost:3000/wwwroot/components/source-ts/demo1-spile/hello-vue.html should work, but the devtools will show you that get requests for polyfill.js, vue.js, system.js, dist-ts/hello-vue.js are failing with 404.
    Next, install some dependencies with npm and get TypeScript watch transpilation going ...

0. $ touch tsconfig.json to make a file in wwwroot/components

0. add the following to wwwroot/components/tsconfig.json

        ```
            {
            "compileOnSave": false,
            "include": [ "source-ts/**/*" ],

            "compilerOptions": {
                "outDir": "dist-ts",
                "module": "system",
                "target": "es5",
                "noImplicitUseStrict": true,
                "allowJs": true,
                "declaration": false,
                "allowUnreachableCode": false,
                "noFallthroughCasesInSwitch": true,
                "noImplicitReturns": true,
                "newLine": "CRLF",
                "removeComments": true,
                "sourceMap": true,
                "noImplicitAny": false,
                "noEmitOnError": true
            }
          }

        ```

  0. add the following to the package.json, and the scripts section 

        ```
        "scripts": {
          "ts-watch": "node ./node_modules/typescript/lib/tsc -p wwwroot/components/tsconfig.json -w"
        },

        ```
    0. Create an empty JavaScript file called, wwwroot/components/source-ts/x.js.  Without this, TypeScript watch does not mirror the directory structure in the source-ts directory.
    
    0. start TypeScript in watch mode, so this it compiles the transpiled .vue files to es5 JavaScript.
    
        ```
        $> npm run ts-watch
        ```

    0. At this stage, the demo1 html should be working at http://localhost:3000/wwwroot/components/source-ts/demo1-spile/hello-world.html. Next, a vue component ...

    0. Create wwwroot/components/source-ts/demo2-VueSystemjs/person.html and add this text, ...

      
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <title></title>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <script src="/node_modules/babel-polyfill/dist/polyfill.min.js"></script>
            <script src="/node_modules/systemjs/dist/system.js"></script>
        </head>

        <body>

        acme-person

            <div id="person" v-cloak>    
                <!-- pass the view instance a view-model called $data to the component called acme-person -->
                <acme-person v-bind:person="$data"></acme-person>
            </div>

            <script>
                System.import("/wwwroot/components/dist-ts/demo2-VueSystemjs/person.js");
            </script>

        </body>
        </html>


  0. Create wwwroot/components/source-ts/demo2-VueSystemjs/person.js and add this text, ...

        ```
          import Vue from "/node_modules/vue/dist/vue.js";

          import "./acme-person.js"

          // vue instance
          let component = {
              el: '#person',
              data: function () {
                  return {
                      surname:'Smith'};
              }
          };

          window.personComponent = new Vue(component);
      ```
    0. create a file called wwwroot/components/source-ts/demo2-VueSystemjs/acme-person.js
        ```
          <template>
              <div>
                  {{person.surname}}
              </div>
          </template>
          <script>

              import Vue from "/node_modules/vue/dist/vue.js";

              // this component takes a property called person as a parameter
              let component = {
                  props: ["person"],
                  template: null
              };

              // after this,  we can use a new tag : <acme-person v-bind:person="$data"></acme-person>
              Vue.component("acme-person", component);

          </script>

        ```


### Demo #3 : Several .vue components in parent-child-grandchild relationship, each component separately responsible for its concern 

Demo 3 : You can copy demo 3 from from the files in in the /node_modules/vue-spile directory. 
Cut and paste the 
\node_modules\vue-spile\wwwroot\components\source-ts\demo3-person 
to your own \vsdemo\wwwroot\components\source-ts directory.
The example uses 3 components, a person vue component, an invoice vue component , and a invoiceitem vue component.
It should work in IE11.


0. copy all the files from  \node_modules\vue-spile\wwwroot\components\source-ts\demo3-person
to \vsdemo\wwwroot\components\source-ts\demo3-person

0. The .vue files should be transpiled as you paste them.

0. open /wwwroot/components/source-ts/demo3-Person/person.html in your browser.
  
0. The person vue component uses the invoice vue component which uses the invoiceitem vue component.

0. Review the example as a start point of for componentised development with Vue.
 