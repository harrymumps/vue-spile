const fs = require('fs')
const path = require('path')
const EventEmitter = require('events')

const compiler = require('vue-template-compiler')

const beautify = require('js-beautify').js_beautify

const VueSpilerCore = require('./VueSpileCore').core

class VueSpiler {

    static getFileContent(filePathName) {
        try {
            let ret = fs.readFileSync(filePathName, `utf8`)
            return ret;
        } catch (e) {
            return null
        }
    }

    static compileWithVueTemplateCompiler(vueFileContent) {

        // get descriptor 
        let descriptor = compiler.parseComponent(vueFileContent)

        // get compiledTemplate 
        if(!descriptor)
        {
            // Sad path
            //console.log('YOW');
            return VueSpilerCore.fail()
        }

        if(!descriptor.template)
        {
            // Sad path
            //console.log('YOW');
            return VueSpilerCore.fail()
        }

        if(!descriptor.template.content)
        {
            // Sad path
            //console.log('YOW');
            return VueSpilerCore.fail()
        }

        let compile = compiler.compile(descriptor.template.content)
        let compileToFunctions = compiler.compileToFunctions(descriptor.template.content)
        let ssrCompileToFunctions = compiler.ssrCompileToFunctions(descriptor.template.content)

        // Return
        return {
            descriptor,
            compile,
            compileToFunctions,
            ssrCompileToFunctions
        }
    }

    static getVueScriptFilename(filePathName) {
        return filePathName.replace(`.vue`, `.js`)
    }

    static getVueStyleFilename(filePathName) {
        return filePathName.replace(`.vue`, `.scss`)
    }

    static ensureFile(newFileName, newFileContent) {
        fs.writeFileSync(newFileName, newFileContent)
    }

    static mungeVueTemplateCompilation(vueTemplateCompilation) {

        var descriptor = vueTemplateCompilation.descriptor

        /*
        res = jsbeautifier.beautify('some javascript', opts)
        */
        //  return jsbeautify(code);
        let vueScriptPre = descriptor.script.content
            
        let opts = {comma_first:true};
        let vueScript = beautify(vueScriptPre,opts);

        let vueTemplate = descriptor.template.content

        let render = vueTemplateCompilation.compile.render

        //console.log(`AAAA`)
        //console.log(vueTemplateCompilation.compile)
        //let render = vueTemplateCompilation.compile
        //console.log(`ZZZZ`)

        // look for the string ... "template : X "
        var matchesForTheTemplateString = vueScript.match(/template\s*:\s*[a-zA-Z]*\s/)

        // matchesFor THEN = {   } 
        // component\s*=\s*{[\s\S]*}
        //var matchesForJsObject = vueScript.match(/component\s*=\s*{[\s\S]*}/)
        //var matchesForJsObject = vueScript.match(/\s*=\s*{[\s\S]*}/)
        // matchesFor  {   } 
        var matchesForJsObject = vueScript.match(/\s*{[\s\S]*}/)

        // case 1 : found string template, so coder wants a template string, not a render function
        // RE-START THEN template THEN any-whitespace THEN : THEN any-whitespace THEN a-z or A-Z THEN any-whitespace THEN RE-END
        if (matchesForTheTemplateString) {
            // the JS code line that pokes in a template back tick string
            const replacement = `template: \`${vueTemplate}\` `

            let ret = vueScript.replace(/template\s*:\s*[a-zA-Z]*\s/, replacement)

            //let opts = {comma_first:true};
            let opts = {comma_first:false};
            let ret_  = beautify(ret,opts);
            return ret_;
        }

        // case 2 : not found string template : 
        //if (matchesForJsObject) {
        //  // the JS code line that pokes in a template back tick string
        //  let replacement = `template: \`${vueTemplate}\` `;
        //
        //  var theMatchedString = matchesForJsObject[0];
        //  
        //  var newVueConfig = theMatchedString.replace("}", `,\n\t${replacement}\n}`);
        //  
        //  let ret = vueScript.replace(/component\s*=\s*{[\s\S]*}/, newVueConfig);
        //  
        //  return ret;
        //}

        // case 3 : put in vue render function
        if (matchesForJsObject) {

            // static render funs
            let rfs = vueTemplateCompilation.compile.staticRenderFns.reduce((acc, x) => `${acc}function anonymous(){${x}},`, ''); 

            // since the function kicked out here uses the JS keyword 'with'
            // See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/with
            // the tsconfig compiler should NOT throw out "use strict" , since the jscode will then except
            // => so for the typscript compiler, use the tsconfig setting  "noImplicitUseStrict": true
            const replacement = `render: function anonymous() { ${render} } `

            const theMatchedString = matchesForJsObject[0]

            const newVueConfig = theMatchedString.replace(`}`, `,\n\t${replacement}\n,staticRenderFns:[${rfs}]}`)

            // Return
            //let ret =  vueScript.replace(/component\s*=\s*{[\s\S]*}/, newVueConfig)
            let ret =  vueScript.replace(/\s*{[\s\S]*}/, newVueConfig)

            //let opts = {comma_first:true};
            let opts = {comma_first:false};
            let ret_  = beautify(ret,opts);
            return ret_;
        }

        // Sad path
        return createFail()
    }

    static vueSpileFiles(vueFiles) {
        return vueFiles.map(vueFile => vueSpileFile(vueFile))
    }

    // list all files in a directory in Node.js recursively,synchronously 
    static walkSync(dir, filelist) {

        files = fs.readdirSync(dir)

        filelist = filelist || []

        files.forEach(function (file) {
            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist = walkSync(path.join(dir, file), filelist)
            } else {
                if (1 === 1) {
                    // todo : apply filter if present ...
                    filelist.push(path.join(dir, file))
                } else {
                    // todo : no filter ...
                    filelist.push(path.join(dir, file))
                }
            }
        });
        return filelist
    }

    static vueSpileFile(vueFile) {

        if (!vueFile) {
            return core.fail()
        }

        if (typeof vueFile !== `string`) {
            return core.fail()
        }

        if (!vueFile.endsWith(`.vue`)) {
            //core.cl({
            //    msg: `${vueFile} is not a .vue file `,
            //    level: core.K_CL_DEV
            //})
            return core.fail()
        }

        core.cl({
            msg: `Spiling ${vueFile} ... ${new Date()}`,
            level: core.K_CL_PROD
        })

        let jsVueFile = VueSpiler.getVueScriptFilename(vueFile)

        let styleVueFile = VueSpiler.getVueStyleFilename(vueFile)

        let fileContent = VueSpiler.getFileContent(vueFile)

        let vueTemplateCompilation = VueSpiler.compileWithVueTemplateCompiler(fileContent)

        let descriptor = vueTemplateCompilation.descriptor

        if(descriptor && descriptor.script  && descriptor.script.content)
        {
            let vueScript = descriptor.script.content
            
            let vueTemplate = descriptor.template.content

            // map the template into the js file content
            let mungedVueScript = VueSpiler.mungeVueTemplateCompilation(vueTemplateCompilation)
            
            // save the js file
            VueSpiler.ensureFile(jsVueFile, mungedVueScript)
            
            // save the style file :: NOTE : it looks like there is an array here!
            let style = ''
            //VueSpiler.ensureFile(styleVueFile, ``)

            let styleFileShouldBeEnsured = false ; 
            if (!!styleVueFile) {
                if (descriptor && descriptor.styles && descriptor.styles[0]) {
                    style = descriptor.styles[0]
                    if (style && style.content) {
                        const styleContent = style.content 
                        console.log(` ** styleContent: [${styleContent}] `);
                        if( styleContent.length > 0 )
                        {
                            styleFileShouldBeEnsured = true ; 
                        }
                    }
                }
            }

            if( styleFileShouldBeEnsured  === true )
            {
                //console.log('** ensure style file');
                VueSpiler.ensureFile(styleVueFile, style.content)
            } 
            else
            {
                //console.log('** delete  style file');
                fs.unlink(styleVueFile,function(err){
                    //if(err) 
                    //{
                    //    //return console.log(err)
                    //}
                    //console.log('file deleted successfully');
                });     
            }


            // cp the scss somewhere else so it can be included in the main css file
            let fileNameOfTheFullPath = path.basename(styleVueFile)
            
            let vueMungerConfig = core.getVueSpileConfig()
            
            let targetPath = vueMungerConfig.styleFileOutput

            //console.log( `** the targetPath is   ${targetPath}` )
            //console.log( `** the targetPath len  ${targetPath.length}` )
            let target = `${targetPath}${fileNameOfTheFullPath}`

            // only copy the style file if the target is something other than '.'
            if( targetPath && (typeof targetPath === 'string' ) && targetPath !== '.' )
            {
                fs.createReadStream(styleVueFile).pipe(fs.createWriteStream(target))
            }

            let message = core.createMessage()
            
            let ret = core.objectAssign(message, {
                jsVueFile
            }, {
                styleVueFile
            }, {
                target
            })
        
            // Return
            return core.success(ret)
        }
    }

    static onFileChange(message) {
        VueSpiler.vueSpileFile(message.fileName)
    }

}

exports.VueSpiler = VueSpiler
